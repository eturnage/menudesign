/*
name: Anthony Truong
course: CST306
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackGeneration : MonoBehaviour
{
    public GameObject track;
    public GameObject[] obstacle;

    private Vector3 position;

    private Transform playerPos;

    private float zAxis = 0;
    private float tileLength = 10;

    private int nbTile = 5;
    private int count = 0;

    void Start()
    {
        position = GameObject.FindGameObjectWithTag("Player").transform.position;
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        for (int i = 0; i < nbTile; i++)
        {
            spawnTile();
        }
    }

    void Update()
    {
        if (playerPos.position.z > (zAxis - nbTile * tileLength))
        {
            spawnTile();
            ++count;
            if (count == 3)
            {
                GameObject obstacl;
                int rnd = Random.Range(0, obstacle.Length - 1);
                obstacl = Instantiate(obstacle[rnd]) as GameObject;
                obstacl.transform.position = Vector3.forward * zAxis;
                obstacl.transform.position += new Vector3(0, 1, 0);
                count = 0;
            }
        }
    }

    private void spawnTile()
    {
        GameObject tile;
        tile = Instantiate(track) as GameObject;
        tile.transform.position = Vector3.forward * zAxis;
        zAxis += tileLength;
    }

    private void useless1()
    {
        int i = 0;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
        ++i;
    }
}