﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button_Manager: MonoBehaviour
{

    public void gameStart()
    {
        SceneManager.LoadScene("test_level");

    }

    public void settingsButton()
    {
        SceneManager.LoadScene("settings_menu");
    }

    public void mainMenuButton()
    {
        SceneManager.LoadScene("mainmenu");
    }

    public void quitGameButton()
    {
        Application.Quit();
    }
}
